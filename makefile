default:
	mkdir -p ./bin
	gcc src/main.c -o bin/SwimmyDud -l raylib -lm
opt:
	mkdir -p ./bin
	gcc src/main.c -o bin/SwimmyDud -l raylib -lm -O3
run:
	mkdir -p ./bin
	gcc src/main.c -o bin/SwimmyDud -l raylib -lm
	./bin/SwimmyDud
runopt:
	mkdir -p ./bin
	gcc src/main.c -o bin/SwimmyDud -l raylib -lm -O3
	./bin/SwimmyDud
debug:
	mkdir -p ./bin
	gcc src/main.c -o bin/SwimmyDud -l raylib -lm -g
	echo "Built SwimmyDud For Debugging."
web:
	mkdir -p ./bin/web
	emcc src/main.c -o bin/web/SwimmyDud.html -I ./include -L ./lib/wasm -D_DEFAULT_SOURCE -DSWIMMYDUD_OS_WASM -lraylib -lglfw -lm -std=c99 -s USE_GLFW=3 -sLLD_REPORT_UNDEFINED --emrun -s EXPORTED_RUNTIME_METHODS=ccall -s ASYNCIFY -sTOTAL_MEMORY=75MB -s FORCE_FILESYSTEM=1 -DPLATFORM_WEB --preload-file res@res
webopt:
	mkdir -p ./bin/web
	emcc src/main.c -o bin/web/SwimmyDud.html -l raylib -lm -O3