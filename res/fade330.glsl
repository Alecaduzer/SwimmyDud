#version 330

precision mediump float;

varying vec2 fragTexCoord;
varying vec4 fragColor;

uniform float fadeAmount;
uniform sampler2D texture0;

//out vec4 finalColor;



void main() {
    float Value = texture2D(texture0, fragTexCoord).r;
     float FloatAlpha = smoothstep(fadeAmount, fadeAmount + 0.25, Value * (1.0 - 0.25) + 0.25);      
     gl_FragColor = vec4(0.0,0.0,0.0, 1.0 - FloatAlpha);
}