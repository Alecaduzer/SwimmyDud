#version 330

precision mediump float;

varying vec2 fragTexCoord;
varying vec4 fragColor;

uniform sampler2D texture0;
uniform sampler2D texture1;

uniform float waterTime;

uniform float distortionScale;
uniform float noiseScale;
uniform float waterSpeed;

//out vec4 finalColor;

void main() {
    float noiseTexel = texture2D(texture1, fragTexCoord * noiseScale - waterTime * waterSpeed).r;
    vec3 frameBufferTexel = texture2D(texture0, fragTexCoord - noiseTexel * distortionScale).rgb;
    gl_FragColor = vec4(frameBufferTexel, 1.0);
}