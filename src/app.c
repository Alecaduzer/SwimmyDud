#include <raylib.h>
#include "app.h"
#include "game.h"
#include "quackedupgui.h"

void app_init() {
    GlobalData->FrameBuffer = LoadRenderTexture(GAME_WIDTH, GAME_HEIGHT);
    
    GlobalData->Assets.MainFont = LoadFont("res/MainFont.ttf");


    GlobalData->Assets.SplashScreen1 = LoadTexture("res/QuackedUpSoftwareLogoV2.png");
    GlobalData->Assets.SplashScreen2 = LoadTexture("res/CraneEngineLogo4.png");

    GlobalData->Assets.SplashScreenSound1 = LoadSound("res/Splash1.wav");
    GlobalData->Assets.SplashScreenSound2 = LoadSound("res/Splash2.wav");


    GlobalData->Assets.Player = LoadTexture("res/dud2.png");
    GlobalData->Assets.DeadDud = LoadTexture("res/duddeath1.png");
    GlobalData->Assets.Shark = LoadTexture("res/shark4.png");
    GlobalData->Assets.Swim = LoadTexture("res/swim.png");



    GlobalData->Assets.DudQuack1 = LoadSound("res/DudQuack1.wav");
    GlobalData->Assets.DudQuack2 = LoadSound("res/DudQuack2.wav");
    GlobalData->Assets.DudQuack3 = LoadSound("res/DudQuack3.wav");
    GlobalData->Assets.DudQuack4 = LoadSound("res/DudQuack4.wav");

    GlobalData->Assets.DudFlap = LoadSound("res/DudFlap2.wav");

    GlobalData->Assets.WaterSounds = LoadMusicStream("res/WaterLoop.wav");

    GlobalData->Assets.PointDing = LoadSound("res/BetterPoint.wav");
    GlobalData->Assets.CapeUnlockDing = LoadSound("res/Unlock.wav");

    GlobalData->Assets.DudDeathSound = LoadSound("res/DudDeath.wav");


    GlobalData->Assets.Cape1 = LoadTexture("res/cape1v2.png");
    GlobalData->Assets.Cape2 = LoadTexture("res/cape2.png");
    GlobalData->Assets.Cape3 = LoadTexture("res/cape3.png");
    GlobalData->Assets.Cape4 = LoadTexture("res/cape4.png");
    GlobalData->Assets.Cape5 = LoadTexture("res/cape5.png");
    GlobalData->Assets.Cape6 = LoadTexture("res/cape6.png");

    GlobalData->Assets.BG0 = LoadTexture("res/bg0.png");
    //SetTextureWrap(GlobalData->Assets.BG0, TEXTURE_WRAP_REPEAT);
    GlobalData->Assets.BG1 = LoadTexture("res/bg1.png");
    //SetTextureWrap(GlobalData->Assets.BG1, TEXTURE_WRAP_REPEAT);
    GlobalData->Assets.BG2 = LoadTexture("res/bg2.png");
    //SetTextureWrap(GlobalData->Assets.BG2, TEXTURE_WRAP_REPEAT);

    GlobalData->DudCam.target = (Vector2){0,0};
    GlobalData->DudCam.zoom = 1;
    GlobalData->DudCam.offset = (Vector2){GAME_WIDTH * 0.5, GAME_HEIGHT * 0.5};

    GlobalData->Player.BoundingBox = (Rectangle){0,0, 16,16};
    GlobalData->Player.Velocity = (Vector2){0,0};
    GlobalData->JumpForce = 111.f;
    GlobalData->Gravity = 301.4f;

    GlobalData->SharkSpeed = 70;
    GlobalData->SharkSpacing = 25.9f;

    GlobalData->SpawnRate = 1.28;
    GlobalData->SpawnTimer = 0;

    GlobalData->Background[0].Pos = 0;
    GlobalData->Background[0].Speed = 0;

    GlobalData->Background[1].Pos = 0;
    GlobalData->Background[1].Speed = 0.036f;

    GlobalData->Background[2].Pos = 0;
    GlobalData->Background[2].Speed = 0.056f;

    load_game();
    GlobalData->SplashScene.Playing = true;

    GlobalData->ShowDebugMenu = false;
    GlobalData->DebuggerRect = (Rectangle){10,10, 400,900};
    GlobalData->DebuggerEnabled = false;

    GlobalData->Assets.FadeShader = LoadShader(0, TextFormat("res/fade%d.glsl", SHADER_VERSION));
    GlobalData->Assets.FadeTexture = LoadTexture("res/shards.png");

    GlobalData->Assets.WaterShader = LoadShader(0, TextFormat("res/watershader%d.glsl", SHADER_VERSION));
    GlobalData->Assets.WaterNoise = LoadTexture("res/waternoise.png");
	SetTextureWrap(GlobalData->Assets.WaterNoise, TEXTURE_WRAP_REPEAT);

    GlobalData->WaterDistortionScale = 0.0032;
    GlobalData->WaterNoiseScale = 15;
    GlobalData->WaterSpeed = 0.168;

    qui_init();

    qui_change_theme(WHITE, BLACK, DARKGRAY, GRAY, 5);
}

void splash_update() {
    if (!GlobalData->SplashScene.Initialized) {
        GlobalData->SplashScene.Initialized = true;
        GlobalData->ScreenFade = 1;
        PlaySound(GlobalData->Assets.SplashScreenSound1);
    }
    GlobalData->SplashScene.PlayHead += GetFrameTime();
    switch (GlobalData->SplashScene.Stage) {
        case 0: {
            GlobalData->ScreenFade -= GetFrameTime();
            if (GlobalData->ScreenFade <= 0) {
                GlobalData->SplashScene.Stage++; 
                GlobalData->SplashScene.PlayHead = 0;
            }
        } break; // LAST HERE ()>:

        case 1: {
            if (GlobalData->SplashScene.PlayHead > 5) {
                GlobalData->SplashScene.Stage++;
            }
        } break;

        case 2: {
            GlobalData->ScreenFade += GetFrameTime();
            if (GlobalData->ScreenFade >= 1) {
                #ifdef BRO_EDITION
                GlobalData->SplashScene.Stage++;
                PlaySound(GlobalData->Assets.SplashScreenSound2);
                #else
                GlobalData->SplashScene.Stage = 5; 
                #endif
            }
        } break;
        
        case 3: {
            GlobalData->ScreenFade -= GetFrameTime();

            if (GlobalData->ScreenFade <= 0) {
                GlobalData->SplashScene.Stage++;
                GlobalData->SplashScene.PlayHead = 0;
                
            }
        } break;

        case 4: {
            if (GlobalData->SplashScene.PlayHead > 5) {
                GlobalData->SplashScene.Stage++;
            }
        } break;

        case 5: {
            GlobalData->ScreenFade += GetFrameTime();

            if (GlobalData->ScreenFade >= 1) {
                GlobalData->SplashScene.Stage++;
                GlobalData->CurrentAppState = APPSTATE_GAMEPLAY;
                GlobalData->ScreenFade = 1;
                SetMusicVolume(GlobalData->Assets.WaterSounds, 0.0f);
                PlayMusicStream(GlobalData->Assets.WaterSounds);
            }
        } break;

        case 6: {
            GlobalData->ScreenFade -= GetFrameTime();

            SetMusicVolume(GlobalData->Assets.WaterSounds, 1 - GlobalData->ScreenFade);

            if (GlobalData->ScreenFade <= 0) {
                GlobalData->SplashScene.Playing = false;
                GlobalData->ScreenFade = 0;
            }
        }
    }   
}

F32 min(F32 a, F32 b) {
    return a < b ? a : b;
}

void splash_draw() {
    BeginDrawing();
    ClearBackground(BLACK);
    DrawRectangle(0,0, GetScreenWidth(), GetScreenHeight(), BLACK);
    F32 ScreenScale1 = min((F32)GetScreenWidth() / (F32)GlobalData->Assets.SplashScreen1.width, (F32)GetScreenHeight() / (F32)GlobalData->Assets.SplashScreen1.height);
    F32 ScreenScale2 = min((F32)GetScreenWidth() / (F32)GlobalData->Assets.SplashScreen2.width, (F32)GetScreenHeight() / (F32)GlobalData->Assets.SplashScreen2.height);

    if (GlobalData->SplashScene.Stage < 3) {
        DrawTexturePro(
            GlobalData->Assets.SplashScreen1,
            (Rectangle){0, 0, GlobalData->Assets.SplashScreen1.width, GlobalData->Assets.SplashScreen1.height},
            (Rectangle) {(GetScreenWidth() - ((float)GlobalData->Assets.SplashScreen1.width * ScreenScale1)) * 0.5,(GetScreenHeight() - ((float)GlobalData->Assets.SplashScreen1.height * ScreenScale1)) * 0.5, (float)GlobalData->Assets.SplashScreen1.width * ScreenScale1, (float)GlobalData->Assets.SplashScreen1.height * ScreenScale1},
            (Vector2){0,0},
            0,
            WHITE
        );
    }

    else {
        DrawTexturePro(
            GlobalData->Assets.SplashScreen2,
            (Rectangle){0, 0, GlobalData->Assets.SplashScreen2.width, GlobalData->Assets.SplashScreen2.height},
            (Rectangle) {(GetScreenWidth() - ((float)GlobalData->Assets.SplashScreen2.width * ScreenScale2)) * 0.5,(GetScreenHeight() - ((float)GlobalData->Assets.SplashScreen2.height * ScreenScale2)) * 0.5, (float)GlobalData->Assets.SplashScreen2.width * ScreenScale2, (float)GlobalData->Assets.SplashScreen2.height * ScreenScale2},
            (Vector2){0,0},
            0,
            WHITE
        );       
    }

    DrawRectangle(0,0, GetScreenWidth(), GetScreenHeight(), Fade(BLACK, GlobalData->ScreenFade));

    EndDrawing();
}

void app_update() {  
    if (GlobalData->SplashScene.Playing) {
        splash_update();
    }

    if (IsKeyPressed(KEY_GRAVE)) {
        GlobalData->ShowDebugMenu = !GlobalData->ShowDebugMenu;
    }
    if (IsKeyDown(KEY_Q) && IsKeyDown(KEY_U) && IsKeyDown(KEY_A) && IsKeyDown(KEY_C) && IsKeyDown(KEY_K)) {
        GlobalData->DebuggerEnabled = true;
        GlobalData->ShowDebugMenu = true;
    }

    switch (GlobalData->CurrentAppState) {
        case APPSTATE_SPLASH: {
            splash_draw();
        } break;

        case APPSTATE_GAMEPLAY: {
            UpdateMusicStream(GlobalData->Assets.WaterSounds);
            game_update();
            game_render();
        } break;
    }
}

void app_terminate() {
    
}