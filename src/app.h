#ifndef _APP_H_
#define _APP_H_

#include "defines.h"

#define GAME_WIDTH 400
#define GAME_HEIGHT 200

#define MAX_SHARKS 32

#define PARALLAX_LAYER_COUNT 3

enum AppStates {
    APPSTATE_SPLASH,
    APPSTATE_GAMEPLAY,
};

typedef struct Entity_Player {
    Rectangle BoundingBox;
    Vector2 Velocity;
} Entity_Player;

typedef struct Entity_Shark {
    Vector2 Pos;
    F32 Spacing;
    B8 IsActive;
    B8 IsPassed;
} Entity_Shark;

typedef struct Save_Data {
    U16 HighScore;
    U64 TimesPlayed;
} Save_Data;

typedef struct Game_Cutscene {
    B8 Initialized;
    F32 PlayHead;
    B8 Playing;
    B8 FadeIn;
} Game_Cutscene;

typedef struct Game_Splash {
    B8 Initialized;
    F32 PlayHead;
    B8 Playing;
    U8 Stage;
} Game_Splash;

typedef struct Game_Assets {
    Font MainFont;


    Texture SplashScreen1;
    Texture SplashScreen2;

    Sound SplashScreenSound1;
    Sound SplashScreenSound2;


    Texture Player;
    Texture DeadDud;

    Sound DudQuack1;
    Sound DudQuack2;
    Sound DudQuack3;
    Sound DudQuack4;

    Sound DudFlap;

    Music WaterSounds;

    Sound PointDing;
    Sound CapeUnlockDing;

    Sound DudDeathSound;

    Texture Shark;
    Texture Swim;

    Texture BG0;
    Texture BG1;
    Texture BG2;

    Texture Cape1;
    Texture Cape2;
    Texture Cape3;
    Texture Cape4;
    Texture Cape5;
    Texture Cape6;

    Shader FadeShader;
    Texture FadeTexture;

    Shader WaterShader;
    Texture WaterNoise;
} Game_Assets;

typedef struct Parallax_Layer {
    F32 Pos;
    F32 Speed;
} Parallax_Layer;

typedef struct Game_Data {
    Entity_Player Player;
    F32 JumpForce;
    F32 Gravity;

    Entity_Shark Sharks[MAX_SHARKS];
    F32 SharkSpeed;

    Parallax_Layer Background[PARALLAX_LAYER_COUNT];

    Save_Data SaveData;
    Game_Assets Assets;

    U16 Score;
    U16 RecentHighScore;
    U16 LastScore;

    Game_Cutscene DeathScene;
    Game_Splash SplashScene;


    B8 IsPaused;
    B8 IsDead;
    B8 IsRunning;

    RenderTexture2D FrameBuffer;

    Camera2D DudCam;

    F32 SpawnRate;
    F32 SpawnTimer; 

    F32 ScreenFade;

    U8 CurrentAppState;

    B8 ShowDebugMenu;
    B8 DebuggerEnabled;
    Rectangle DebuggerRect;

    F32 SharkSpacing;

    F32 WaterDistortionScale;
    F32 WaterNoiseScale;
    F32 WaterSpeed;
} Game_Data;

Game_Data *GlobalData;

void app_init();
void app_update();
void app_terminate();

#endif