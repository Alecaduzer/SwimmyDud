#include <raylib.h>
#include <string.h>
#include <math.h>

#include "app.h"
#include "game.h"

#include "quackedupgui.h"

U32 max(U32 a, U32 b) {
    return a > b ? a : b;
}

F32 absolute(F32 a) {
    return a < 0 ? -a : a;
}

void calculate_scores() {
    GlobalData->RecentHighScore = max(GlobalData->Score, GlobalData->RecentHighScore);
    GlobalData->SaveData.HighScore = max(GlobalData->RecentHighScore, GlobalData->SaveData.HighScore);
    GlobalData->LastScore = GlobalData->Score;
}

void save_game() {
    if (!GlobalData->DebuggerEnabled) {
        SaveFileData("SaveData.duckdata", &GlobalData->SaveData, sizeof(Save_Data));
    }
}

void load_game() { // FIXME: Causes a Segmentation success
    unsigned char *FileData = LoadFileData("SaveData.duckdata", &GlobalData->SaveData);
    if (FileData)
        memcpy(&GlobalData->SaveData, FileData, sizeof(Save_Data));
}

void spawn_shark() {
    for (int i = 0; i < MAX_SHARKS; i++) {
        Entity_Shark *CurrentShark = &GlobalData->Sharks[i];
        if (!CurrentShark->IsActive) {
            CurrentShark->IsActive = true;
            CurrentShark->Pos.x = GAME_WIDTH;
            CurrentShark->Pos.y = GetRandomValue(-GAME_HEIGHT / 2 + 75, GAME_HEIGHT / 2 - 85);
            CurrentShark->Spacing = GlobalData->SharkSpacing;
            CurrentShark->IsPassed = false;
            break;
        }
    }
}

void process_sharks() {
    for (int i = 0; i < MAX_SHARKS; i++) {
        if (GlobalData->Sharks[i].IsActive) {
            GlobalData->Sharks[i].Pos.x -= GlobalData->SharkSpeed * GetFrameTime();
        }
        if (GlobalData->Sharks[i].Pos.x < -GAME_WIDTH / 2 - 30) {
            GlobalData->Sharks[i].IsActive = false;
        }
    }
}

void apply_player_gravity() {
    GlobalData->Player.Velocity.y += GlobalData->Gravity * GetFrameTime();
}

B8 player_input() {
    return IsKeyPressed(KEY_SPACE) || IsMouseButtonPressed(MOUSE_BUTTON_LEFT) || IsKeyPressed(KEY_UP) || (GetTouchPointCount() > 0);
}

void play_jumping_noise() {
    PlaySoundMulti(GlobalData->Assets.DudFlap);

    if (GetRandomValue(0, 1)) {
        switch (GetRandomValue(0, 3)) {
            case 0: {
                PlaySoundMulti(GlobalData->Assets.DudQuack1);
            } break;
            
            case 1: {
                PlaySoundMulti(GlobalData->Assets.DudQuack2);
            } break;

            case 2: {
                PlaySoundMulti(GlobalData->Assets.DudQuack3);
            } break;

            case 3: {
                PlaySoundMulti(GlobalData->Assets.DudQuack4);
            } break;
        }
    }
}

void player_jump() {
    play_jumping_noise();
    GlobalData->Player.Velocity.y = -GlobalData->JumpForce;
}

void apply_player_motion() {
    GlobalData->Player.BoundingBox.y += GlobalData->Player.Velocity.y * GetFrameTime();
}

B8 death_clamp() {
    return GlobalData->Player.BoundingBox.y > GAME_HEIGHT / 2 || GlobalData->Player.BoundingBox.y < -GAME_HEIGHT / 2;
}

B8 check_shark_collisions() {
    for (int i = 0; i < MAX_SHARKS; i++) {
        Entity_Shark *SharkGill = &GlobalData->Sharks[i];
        if (SharkGill->IsActive) {
            Rectangle PlayerBounds = (Rectangle){
                GlobalData->Player.BoundingBox.x - GlobalData->Player.BoundingBox.width / 2,
                GlobalData->Player.BoundingBox.y - GlobalData->Player.BoundingBox.height / 2,
                GlobalData->Player.BoundingBox.width,
                GlobalData->Player.BoundingBox.height
            };
            if (CheckCollisionRecs(PlayerBounds, (Rectangle){SharkGill->Pos.x - 25 * 0.5, SharkGill->Pos.y - SharkGill->Spacing - 70, 25, 70})) {
                return true;
            }
            else if (CheckCollisionRecs(PlayerBounds, (Rectangle){SharkGill->Pos.x - 25 * 0.5, SharkGill->Pos.y + SharkGill->Spacing, 25, 70})) {
                return true;
            } 
        }

    } 
    return false;
}

void check_player_passing() {
    for (int i = 0; i < MAX_SHARKS; i++) {
        if (GlobalData->Sharks[i].Pos.x < GlobalData->Player.BoundingBox.x && !GlobalData->Sharks[i].IsPassed) {
            GlobalData->Sharks[i].IsPassed = true;
            GlobalData->Score++;

            switch (GlobalData->Score) {
                case 150: {
                    PlaySound(GlobalData->Assets.CapeUnlockDing);
                } break;

                case 100: {
                    PlaySound(GlobalData->Assets.CapeUnlockDing);
                } break;

                case 75: {
                    PlaySound(GlobalData->Assets.CapeUnlockDing);
                } break;

                case 50: {
                    PlaySound(GlobalData->Assets.CapeUnlockDing);
                } break;

                case 30: {
                    PlaySound(GlobalData->Assets.CapeUnlockDing);
                } break;

                case 15: {
                    PlaySound(GlobalData->Assets.CapeUnlockDing);
                } break;

                default: {
                    PlaySound(GlobalData->Assets.PointDing);
                } break;
            }
        }
    }
}

void process_death_scene() {
    if (!GlobalData->DeathScene.Initialized) {
        GlobalData->DeathScene.Initialized = true;
        GlobalData->DeathScene.PlayHead = 0;
        GlobalData->Player.Velocity.y = -300;
        GlobalData->DeathScene.FadeIn = true;
        PlaySound(GlobalData->Assets.DudDeathSound);
    }

    if (GlobalData->DeathScene.PlayHead > 2.25 && GlobalData->DeathScene.FadeIn) {
        GlobalData->ScreenFade += 2 * GetFrameTime();
    }

    if (GlobalData->ScreenFade >= 1 && GlobalData->DeathScene.FadeIn) {
        GlobalData->DeathScene.FadeIn = false;
        game_reset();
    }

    GlobalData->DeathScene.PlayHead += GetFrameTime();

    if (!GlobalData->DeathScene.FadeIn) {
        GlobalData->ScreenFade -= 2 * GetFrameTime();
    }

    if (GlobalData->ScreenFade <= 0 && !GlobalData->DeathScene.FadeIn) {
        GlobalData->DeathScene.Initialized = false;
        GlobalData->DeathScene.Playing = false;
        GlobalData->ScreenFade = 0;
    }

}

void draw_logo() {
    DrawTexturePro(
        GlobalData->Assets.Swim,
        (Rectangle){0,0, GlobalData->Assets.Swim.width, GlobalData->Assets.Swim.height},
        (Rectangle){GetScreenWidth() / 2, 30, 338, 150},
        (Vector2){338 * 0.5,0},
        0,
        WHITE
    );
}

void draw_stats() {
    DrawTextPro(
        GlobalData->Assets.MainFont,
        TextFormat("Score: %d", GlobalData->LastScore),
        (Vector2){GetScreenWidth() / 2, 200},
        (Vector2){MeasureTextEx(GlobalData->Assets.MainFont, TextFormat("Score: %d", GlobalData->LastScore), 25, 2).x / 2, 0},
        0,
        25,
        2,
        WHITE
    );

    DrawTextPro(
        GlobalData->Assets.MainFont,
        TextFormat("RecentHighScore: %d",GlobalData->RecentHighScore),
        (Vector2){GetScreenWidth() / 2, 230},
        (Vector2){MeasureTextEx(GlobalData->Assets.MainFont, TextFormat("RecentHighScore: %d", GlobalData->RecentHighScore), 25, 2).x / 2, 0},
        0,
        25,
        2,
        WHITE
    );
	#ifndef SWIMMYDUD_OS_WASM
    DrawTextPro(
        GlobalData->Assets.MainFont,
        TextFormat("AllTimeHighScore: %d", GlobalData->SaveData.HighScore),
        (Vector2){GetScreenWidth() / 2, 260},
        (Vector2){MeasureTextEx(GlobalData->Assets.MainFont, TextFormat("AllTimeHighScore: %d", GlobalData->SaveData.HighScore), 25, 2).x / 2, 0},
        0,
        25,
        2,
        WHITE
    ); 
	#endif

    DrawTextPro(
        GlobalData->Assets.MainFont,
        TextFormat("Times Played: %ld", GlobalData->SaveData.TimesPlayed),
        (Vector2){GetScreenWidth(), GetScreenHeight()},
        MeasureTextEx(GlobalData->Assets.MainFont, TextFormat("Times Played: %ld", GlobalData->SaveData.TimesPlayed), 30, 1),
        0,
        30,
        1,
        (Color){0, 255, 103, 255}
    );

    DrawTextPro(
        GlobalData->Assets.MainFont,
        "Swimmy Dud V1.0",
        (Vector2){sinf(GetTime() * 6 + 2) * 12 + 100, GetScreenHeight()},
        (Vector2){sinf(GetTime() + 2 * 6) * 12 + 100, MeasureTextEx(GlobalData->Assets.MainFont, "Swimmy Dud V1.0", 20, 1).y + 200},
        sinf(GetTime() * 6) + 1 * 13,
        sinf(GetTime() * 6) + 3 * 13,
        sinf(GetTime() * 6) + 1 * 14,
        (Color){sinf(GetTime() * 0.3) * 254 + 1, sinf(GetTime() * 0.7) * 254 + 1, sinf(GetTime() * 1) * 254 + 1, 255}
    );

    DrawTextEx(
        GlobalData->Assets.MainFont,
        "Hold down the keys Q U A C K to cheat",
        (Vector2){GetScreenWidth() / 2 - MeasureTextEx(GlobalData->Assets.MainFont, "Hold down the keys Q U A C K to cheat", 15, 1).x / 2 - 575, 5},
        15,
        1,
        SKYBLUE
    );
}

void draw_pulsating_text() {
    DrawTextPro(
        GlobalData->Assets.MainFont,
        "Press Space to Play!",
        (Vector2){GetScreenWidth() / 2, GetScreenHeight() / 2 + 100},
        (Vector2){MeasureTextEx(GlobalData->Assets.MainFont, "Press Space to Play!", 35, 2).x / 2, 0},
        0,
        35,
        2,
        Fade(WHITE, absolute(sinf(GetTime() * 5)))
    );

}

void draw_peepeepoopoo() {

}

void game_reset() {

    calculate_scores();

    GlobalData->SaveData.TimesPlayed++;
    save_game();

    GlobalData->IsRunning = false;
    GlobalData->IsDead = false;
    GlobalData->Score = 0;
    GlobalData->Player.Velocity = (Vector2){0,0};
    GlobalData->Player.BoundingBox.x = 0;
    GlobalData->Player.BoundingBox.y = 0;

    GlobalData->SpawnTimer = 0;

    for (int i = 0; i < MAX_SHARKS; i++) {
        Entity_Shark *SharkTongue = &GlobalData->Sharks[i];

        SharkTongue->IsActive = false;
    }
}

void draw_sharks() {
    for(int i = 0; i < MAX_SHARKS; i++) {
        Entity_Shark *SharkNose = &GlobalData->Sharks[i]; 
        if (GlobalData->Sharks[i].IsActive) {
            DrawTexturePro(
                GlobalData->Assets.Shark,
                (Rectangle){0,0, GlobalData->Assets.Shark.width, GlobalData->Assets.Shark.height},
                (Rectangle){SharkNose->Pos.x - 25 * 0.5, SharkNose->Pos.y - SharkNose->Spacing - 2.5 * sinf(GetTime() * 3 + i) - 70, 25, 70},
                (Vector2){0,0},
                0,
                WHITE
            );
            DrawTexturePro(
                GlobalData->Assets.Shark,
                (Rectangle){0,0, GlobalData->Assets.Shark.width, -GlobalData->Assets.Shark.height},
                (Rectangle){SharkNose->Pos.x - 25 * 0.5, SharkNose->Pos.y + SharkNose->Spacing + 2.5 * sinf(GetTime() * 3 - i), 25, 70},
                (Vector2){0,0},
                0,
                WHITE
            );
        }
    }
}

void draw_player() {
    if (!GlobalData->IsDead) {
        DrawTexturePro(
            GlobalData->Assets.Player, 
            (Rectangle){0,0, GlobalData->Assets.Player.width, GlobalData->Assets.Player.height},
            GlobalData->Player.BoundingBox,
            (Vector2){GlobalData->Player.BoundingBox.width / 2, GlobalData->Player.BoundingBox.height / 2},
            GlobalData->Player.Velocity.y * 0.3,
            WHITE
        );

        if (GlobalData->SaveData.HighScore >= 150 || GlobalData->Score >= 150) {
            DrawTexturePro(
                GlobalData->Assets.Cape6, 
                (Rectangle){0,0, GlobalData->Assets.Player.width, GlobalData->Assets.Player.height},
                GlobalData->Player.BoundingBox,
                (Vector2){GlobalData->Player.BoundingBox.width / 2, GlobalData->Player.BoundingBox.height / 2},
                GlobalData->Player.Velocity.y * 0.3,
                WHITE
            );
        }
        else if (GlobalData->SaveData.HighScore >= 100 || GlobalData->Score >= 100) {
            DrawTexturePro(
                GlobalData->Assets.Cape5, 
                (Rectangle){0,0, GlobalData->Assets.Player.width, GlobalData->Assets.Player.height},
                GlobalData->Player.BoundingBox,
                (Vector2){GlobalData->Player.BoundingBox.width / 2, GlobalData->Player.BoundingBox.height / 2},
                GlobalData->Player.Velocity.y * 0.3,
                WHITE
            );
        }
        else if (GlobalData->SaveData.HighScore >= 75 || GlobalData->Score >= 75) {
            DrawTexturePro(
                GlobalData->Assets.Cape4, 
                (Rectangle){0,0, GlobalData->Assets.Player.width, GlobalData->Assets.Player.height},
                GlobalData->Player.BoundingBox,
                (Vector2){GlobalData->Player.BoundingBox.width / 2, GlobalData->Player.BoundingBox.height / 2},
                GlobalData->Player.Velocity.y * 0.3,
                WHITE
            );
        }
        else if (GlobalData->SaveData.HighScore >= 50 || GlobalData->Score >= 50) {
            DrawTexturePro(
                GlobalData->Assets.Cape3, 
                (Rectangle){0,0, GlobalData->Assets.Player.width, GlobalData->Assets.Player.height},
                GlobalData->Player.BoundingBox,
                (Vector2){GlobalData->Player.BoundingBox.width / 2, GlobalData->Player.BoundingBox.height / 2},
                GlobalData->Player.Velocity.y * 0.3,
                WHITE
            );
        }
        else if (GlobalData->SaveData.HighScore >= 30 || GlobalData->Score >= 30) {
            DrawTexturePro(
                GlobalData->Assets.Cape2, 
                (Rectangle){0,0, GlobalData->Assets.Player.width, GlobalData->Assets.Player.height},
                GlobalData->Player.BoundingBox,
                (Vector2){GlobalData->Player.BoundingBox.width / 2, GlobalData->Player.BoundingBox.height / 2},
                GlobalData->Player.Velocity.y * 0.3,
                WHITE
            );
        }
        else if (GlobalData->SaveData.HighScore >= 15 || GlobalData->Score >= 15) {
            DrawTexturePro(
                GlobalData->Assets.Cape1, 
                (Rectangle){0,0, GlobalData->Assets.Player.width, GlobalData->Assets.Player.height},
                GlobalData->Player.BoundingBox,
                (Vector2){GlobalData->Player.BoundingBox.width / 2, GlobalData->Player.BoundingBox.height / 2},
                GlobalData->Player.Velocity.y * 0.3,
                WHITE
            );
        }
    }

    else {
        DrawTexturePro(
            GlobalData->Assets.DeadDud, 
            (Rectangle){0,0, GlobalData->Assets.DeadDud.width, GlobalData->Assets.DeadDud.height},
            GlobalData->Player.BoundingBox,
            (Vector2){GlobalData->Player.BoundingBox.width / 2, GlobalData->Player.BoundingBox.height / 2},
            GlobalData->Player.Velocity.y * 2,
            WHITE
        );
    }
}



void update_background() {
    for (U32 i = 0; i < PARALLAX_LAYER_COUNT; i++) {
        if (GlobalData->Background[i].Pos < -1) {
            GlobalData->Background[i].Pos += 1;
        }

        GlobalData->Background[i].Pos -= GetFrameTime() * GlobalData->Background[i].Speed;
    }
}

void draw_background() {
    DrawTextureTiled(
        GlobalData->Assets.BG0,
        (Rectangle){0, 0, GlobalData->Assets.BG0.width, GlobalData->Assets.BG0.height},
        (Rectangle){GlobalData->Background[0].Pos * GlobalData->Assets.BG0.width, 0, GlobalData->Assets.BG0.width, GlobalData->Assets.BG0.height},
        (Vector2){GAME_WIDTH / 2, GAME_HEIGHT / 2},
        0,
        1,
        WHITE
    );

    DrawTextureTiled(
        GlobalData->Assets.BG1,
        (Rectangle){0, 0, GlobalData->Assets.BG1.width, GlobalData->Assets.BG1.height},
        (Rectangle){GlobalData->Background[1].Pos * GlobalData->Assets.BG1.width, 0, GlobalData->Assets.BG1.width * 2, GlobalData->Assets.BG1.height},
        (Vector2){GAME_WIDTH / 2, GAME_HEIGHT / 2},
        0,
        1,
        WHITE
    );

    DrawTextureTiled(
        GlobalData->Assets.BG2,
        (Rectangle){0, 0, GlobalData->Assets.BG2.width, GlobalData->Assets.BG2.height},
        (Rectangle){GlobalData->Background[2].Pos * GlobalData->Assets.BG2.width, 0, GlobalData->Assets.BG2.width * 2, GlobalData->Assets.BG2.height},
        (Vector2){GAME_WIDTH / 2, GAME_HEIGHT / 2},
        0,
        1,
        WHITE
    );
}

void draw_framebuffer() {
    F32 ScreenScale = (F32){GetScreenHeight()} / GAME_HEIGHT; 
    DrawTexturePro(
        GlobalData->FrameBuffer.texture,
        (Rectangle){0,0, GlobalData->FrameBuffer.texture.width, -GlobalData->FrameBuffer.texture.height},
        (Rectangle){
            GetScreenWidth() * 0.5, 0,
            GlobalData->FrameBuffer.texture.width * ScreenScale,
            GlobalData->FrameBuffer.texture.height * ScreenScale,
        },
        (Vector2){GlobalData->FrameBuffer.texture.width * 0.5 * ScreenScale,0}, 0, WHITE
    );

}

void degooser_panel() {
    if (qui_window(&GlobalData->DebuggerRect, "DeGooser Version 1 . 9 . 2. 5", true)) {
        GlobalData->ShowDebugMenu = false;
    }

    F32 LayoutPos = GlobalData->DebuggerRect.y + 40;
    F32 LayoutInterval = 50;

    F32 LayoutX = GlobalData->DebuggerRect.x + 10;

    qui_slider(&GlobalData->SharkSpacing, 100, (Rectangle){LayoutX, LayoutPos, 250, 25}, "SharkSpacing");
    LayoutPos += LayoutInterval;

    qui_slider(&GlobalData->SharkSpeed, 250, (Rectangle){LayoutX, LayoutPos, 250, 25}, "SharkSpeed");
    LayoutPos += LayoutInterval;

    qui_slider(&GlobalData->SpawnRate, 5, (Rectangle){LayoutX, LayoutPos, 250, 25}, "SharkSpawnRate");
    LayoutPos += LayoutInterval * 2;

    qui_slider(&GlobalData->Gravity, 550, (Rectangle){LayoutX, LayoutPos, 250, 25}, "PlayerGravity");
    LayoutPos += LayoutInterval;

    qui_slider(&GlobalData->JumpForce, 250, (Rectangle){LayoutX, LayoutPos, 250, 25}, "PlayerJumpForce");
    LayoutPos += LayoutInterval * 2;

    qui_slider(&GlobalData->Background[1].Speed, 1, (Rectangle){LayoutX, LayoutPos, 250, 25}, "Middle BG Speed");
    LayoutPos += LayoutInterval;

    qui_slider(&GlobalData->Background[2].Speed, 1, (Rectangle){LayoutX, LayoutPos, 250, 25}, "Front BG Speed");
    LayoutPos += LayoutInterval * 2;

    qui_slider(&GlobalData->WaterDistortionScale, 0.4, (Rectangle){LayoutX, LayoutPos, 250, 25}, "WaterDistortion");
    LayoutPos += LayoutInterval;

    qui_slider(&GlobalData->WaterNoiseScale, 15, (Rectangle){LayoutX, LayoutPos, 250, 25}, "WaterNoiseScale");
    LayoutPos += LayoutInterval;

    qui_slider(&GlobalData->WaterSpeed, 2, (Rectangle){LayoutX, LayoutPos, 250, 25}, "WaterSpeed");
    LayoutPos += LayoutInterval * 2;

    qui_label("Press the ~ key to hide", LayoutX, LayoutPos, 25);
    LayoutPos += LayoutInterval;

    qui_label("NOTE: High Scores will not be saved \nwhile hacks are enabled. \nYou will also need to restart \nthe game to disable hack mode.", LayoutX, LayoutPos, 15);

  //  qui_slider(&GlobalData->SharkSpacingTestValue, 75, (Rectangle){GlobalData->DebuggerRect.x + 5, GlobalData->DebuggerRect.y + 100, 100, 25}, "Shark Spacing");
 //   if (qui_button((Rectangle){GlobalData->DebuggerRect.x + 5, GlobalData->DebuggerRect.y + 160, 100, 100}, "Kill Dud", 20)) GlobalData->IsDead = true;
}
    

void game_update() {
    if (!GlobalData->IsDead && GlobalData->IsRunning) {
        GlobalData->SpawnTimer += GetFrameTime();

        if (GlobalData->SpawnRate < GlobalData->SpawnTimer) {
            GlobalData->SpawnTimer -= GlobalData->SpawnRate;
            spawn_shark();
        }
    }
    if (!GlobalData->IsDead) {
        update_background();
        process_sharks();
    }


    if (GlobalData->IsRunning) {
        apply_player_gravity();
        apply_player_motion();
    }

    if (!GlobalData->IsDead) {
        if (player_input()) {
            GlobalData->IsRunning = true;
            player_jump();
        }
    }

    if (death_clamp() || check_shark_collisions()) {
        GlobalData->IsDead = true;
    }

    check_player_passing();

    if (GlobalData->IsDead) {
        GlobalData->DeathScene.Playing = true;
    }

    if (GlobalData->DeathScene.Playing) {
        process_death_scene();
    }
}

void game_render() {
    U32 FadeLocation = GetShaderLocation(GlobalData->Assets.FadeShader, "fadeAmount");
    U32 WaterTimeLocation = GetShaderLocation(GlobalData->Assets.WaterShader, "waterTime");
    U32 WaterNoiseTextureLocation = GetShaderLocation(GlobalData->Assets.WaterShader, "texture1");
    U32 WaterNoiseScaleLocation = GetShaderLocation(GlobalData->Assets.WaterShader, "noiseScale");
    U32 WaterDistortionScaleLocation = GetShaderLocation(GlobalData->Assets.WaterShader, "distortionScale");
    U32 WaterSpeedLocation = GetShaderLocation(GlobalData->Assets.WaterShader, "waterSpeed");

    SetShaderValue(GlobalData->Assets.WaterShader, WaterNoiseScaleLocation, &GlobalData->WaterNoiseScale, SHADER_UNIFORM_FLOAT);
    SetShaderValue(GlobalData->Assets.WaterShader, WaterDistortionScaleLocation, &GlobalData->WaterDistortionScale, SHADER_UNIFORM_FLOAT);
    SetShaderValue(GlobalData->Assets.WaterShader, WaterSpeedLocation, &GlobalData->WaterSpeed, SHADER_UNIFORM_FLOAT);
 
    F32 WaterTime = GetTime();
  
  //  U32 MaskLocation = GetShaderLocation(GlobalData->Assets.FadeShader, "text");
   SetShaderValue(GlobalData->Assets.FadeShader, FadeLocation, &GlobalData->ScreenFade, SHADER_UNIFORM_FLOAT);
//    SetShaderValueTexture(GlobalData->Assets.FadeShader, MaskLocation, GlobalData->Assets.FadeTexture);

    BeginTextureMode(GlobalData->FrameBuffer);
    ClearBackground(BLACK);
        BeginMode2D(GlobalData->DudCam);
            draw_background();
            draw_sharks();
            draw_player();
        EndMode2D();
        if (!GlobalData->IsDead) {
            DrawRectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, (Color){0, 70, 255, 60});
        }
        else {
            DrawRectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, (Color){255, 1, 1, 120});
        }
    EndTextureMode();

    BeginDrawing();
        ClearBackground(BLACK);
        BeginShaderMode(GlobalData->Assets.WaterShader);
         SetShaderValue(GlobalData->Assets.WaterShader, WaterTimeLocation, &WaterTime, SHADER_UNIFORM_FLOAT);
           SetShaderValueTexture(GlobalData->Assets.WaterShader, WaterNoiseTextureLocation, GlobalData->Assets.WaterNoise);
        draw_framebuffer();
        EndShaderMode();

        if (GlobalData->IsRunning) {
            DrawTextPro(
                GlobalData->Assets.MainFont,
                TextFormat("%d", GlobalData->Score), 
                (Vector2){GetScreenWidth() / 2,10},
                (Vector2){MeasureTextEx(GlobalData->Assets.MainFont, TextFormat("%d", GlobalData->Score), 75, 20).x / 2,0},
                0, 
                75, 
                20, 
                WHITE
            );
        }

        else {
            draw_logo();
            draw_stats();
            draw_pulsating_text();
            draw_peepeepoopoo();
        }

        BeginShaderMode(GlobalData->Assets.FadeShader);
        DrawTexturePro(
            GlobalData->Assets.FadeTexture,
            (Rectangle){0,0, GlobalData->Assets.FadeTexture.width, GlobalData->Assets.FadeTexture.height},
            (Rectangle){0,0, GetScreenWidth(), GetScreenHeight()},
            (Vector2){0,0},
            0,
            WHITE
        );
        EndShaderMode();

        if (GlobalData->ShowDebugMenu && GlobalData->DebuggerEnabled) {
            degooser_panel();
        }
        
    EndDrawing();
}
