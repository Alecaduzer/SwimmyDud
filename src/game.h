#ifndef _GAME_H_
#define _GAME_H_

void load_game();
void game_update();
void game_render();
void game_reset();

#endif