#include <raylib.h>
#include <stdlib.h>
#define QUI_IMPLEMENTATION
#include "quackedupgui.h"
#undef QUI_IMPLEMENTATION

#ifdef SWIMMYDUD_OS_WASM
#include <emscripten.h>

#define SHADER_VERSION 100
#endif

#if !defined(SHADER_VERSION)
#define SHADER_VERSION 330
#endif

#include "app.h"
#include "app.c"

#include "game.h"
#include "game.c"

int main(int argc, char **argv) {
    InitWindow(1600,900, "Swimmmy Dud V1.0");
    SetTargetFPS(144);
    SetWindowState(FLAG_WINDOW_RESIZABLE | FLAG_WINDOW_UNDECORATED);
    SetTraceLogLevel(LOG_NONE);

    InitAudioDevice();

    MaximizeWindow();

    GlobalData = calloc(1, sizeof(Game_Data));

    app_init();

    #ifdef SWIMMYDUD_OS_WASM
    emscripten_set_main_loop(app_update, 0, 1);
    #else
    while (!WindowShouldClose()) {
        app_update();
    }
    #endif

    app_terminate();

    free(GlobalData);
}
