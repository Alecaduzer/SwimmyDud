#ifndef _QUI_H_
#define _QUI_H_

#include <raylib.h>
#include <stdio.h>

#define QUI_INTERNAL static
#define QUI_API

enum ButtonStates {
    BUTTONSTATE_NORMAL,
    BUTTONSTATE_FOCUSED,
    BUTTONSTATE_DOWN
};

enum CheckBoxStates {
    CHECKBOXSTATE_NORMAL,
    CHECKBOXSTATE_FOCUSED,
    CHECKBOXSTATE_DOWN
};

enum SliderStates {
    SLIDERSTATE_NORMAL,
    SLIDERSTATE_FOCUSED,
    SLIDERSTATE_DOWN
};

typedef struct I_QUI__Theme {
    Color PrimaryColor;
    Color SecondaryColor;
    Color TextColor;
    Color DetailColor;

    int BorderWidth;
} I_QUI__Theme;

// TextColor
// BorderColor
// CenterColor

QUI_API void qui_reset_theme();
QUI_API void qui_change_theme(Color PrimaryColor, Color SecondaryColor, Color DetailColor, Color TextColor, int BorderWidth);

QUI_API bool qui_button(Rectangle ButtonDimensions, const char *Text, int FontSize);
QUI_API bool qui_button_color_pro(Rectangle ButtonDimensions, const char *Text, int FontSize, Color NormalColor, Color NormalBorderColor, Color FocusedColor, Color FocusedBorderColor, Color PressedColor, Color PressedBorderColor, Color NormalTextColor, Color FocusedTextColor, Color PressedTextColor);
QUI_API void qui_checkbox(Rectangle Dimensions, bool *State, const char *Text);
QUI_API bool qui_window(Rectangle *Dimensions, const char *Text, bool Dragable);
QUI_API void qui_slider(float *Value, float MaxValue, Rectangle Dimensions, const char *Text);

QUI_API void qui_label(const char *Text, int PosX, int PosY, int FontSize);

QUI_API void qui_init();

#ifdef QUI_IMPLEMENTATION

QUI_INTERNAL I_QUI__Theme I_QUITheme;


QUI_API void qui_reset_theme() {
    I_QUITheme.PrimaryColor = WHITE;
    I_QUITheme.SecondaryColor = LIGHTGRAY;
    I_QUITheme.TextColor = BLACK;
    I_QUITheme.DetailColor = SKYBLUE;

    I_QUITheme.BorderWidth = 5;
}

QUI_API void qui_change_theme(Color PrimaryColor, Color SecondaryColor, Color DetailColor, Color TextColor, int BorderWidth) {
    I_QUITheme.PrimaryColor = PrimaryColor;
    I_QUITheme.SecondaryColor = SecondaryColor;
    I_QUITheme.DetailColor = DetailColor;
    I_QUITheme.TextColor = TextColor;
    I_QUITheme.BorderWidth = BorderWidth;
}

QUI_API bool qui_button(Rectangle ButtonDimensions, const char *Text, int FontSize) { // A basic GUI button. Uses the current theme for color
    int ButtonState;
    bool Result = 0;

    ButtonState = BUTTONSTATE_NORMAL;

    if (CheckCollisionPointRec(GetMousePosition(), ButtonDimensions)) {
        ButtonState = BUTTONSTATE_FOCUSED;

        Result = 0;

        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT)) {
            ButtonState = BUTTONSTATE_DOWN;
            Result = 0;
        }
        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
            Result = 1;
        }
    }
    // Draw the button
    switch (ButtonState) {
        case BUTTONSTATE_NORMAL:
            DrawRectangle(ButtonDimensions.x, ButtonDimensions.y, ButtonDimensions.width, ButtonDimensions.height, I_QUITheme.DetailColor); // Draw Borders
            DrawRectangle(ButtonDimensions.x + 5, ButtonDimensions.y + 5, ButtonDimensions.width - 10, ButtonDimensions.height - 10, I_QUITheme.PrimaryColor); // Draw Center
            DrawText(Text, ButtonDimensions.x + 5 + 2, ButtonDimensions.y + 5 + 2, FontSize, I_QUITheme.TextColor);
            break;
        case BUTTONSTATE_FOCUSED:
            DrawRectangle(ButtonDimensions.x, ButtonDimensions.y, ButtonDimensions.width, ButtonDimensions.height, (Color){I_QUITheme.DetailColor.r, I_QUITheme.DetailColor.g, I_QUITheme.DetailColor.b, I_QUITheme.DetailColor.a - 100}); // Draw Borders
            DrawRectangle(ButtonDimensions.x + 5, ButtonDimensions.y + 5, ButtonDimensions.width - 10, ButtonDimensions.height - 10, (Color){I_QUITheme.PrimaryColor.r, I_QUITheme.PrimaryColor.g, I_QUITheme.PrimaryColor.b, I_QUITheme.PrimaryColor.a - 100}); // Draw Center
            DrawText(Text, ButtonDimensions.x + 5 + 2, ButtonDimensions.y + 5 + 2, FontSize, I_QUITheme.TextColor);
            break;
        case BUTTONSTATE_DOWN:
            DrawRectangle(ButtonDimensions.x, ButtonDimensions.y, ButtonDimensions.width, ButtonDimensions.height, (Color){I_QUITheme.PrimaryColor.r, I_QUITheme.PrimaryColor.g, I_QUITheme.PrimaryColor.b, I_QUITheme.PrimaryColor.a - 100}); // Draw Borders
            DrawRectangle(ButtonDimensions.x + 5, ButtonDimensions.y + 5, ButtonDimensions.width - 10, ButtonDimensions.height - 10, (Color){I_QUITheme.SecondaryColor.r, I_QUITheme.SecondaryColor.g, I_QUITheme.SecondaryColor.b, I_QUITheme.SecondaryColor.a - 100}); // Draw Center
            DrawText(Text, ButtonDimensions.x + 5 + 2, ButtonDimensions.y + 5 + 2, FontSize,  I_QUITheme.PrimaryColor);
            break;
    }

    return Result;
}

QUI_API bool qui_button_color_pro(Rectangle ButtonDimensions, const char *Text, int FontSize, Color NormalColor, Color NormalBorderColor, Color FocusedColor, Color FocusedBorderColor, Color PressedColor, Color PressedBorderColor, Color NormalTextColor, Color FocusedTextColor, Color PressedTextColor) {
    int ButtonState;
    bool Result = 0;

    ButtonState = BUTTONSTATE_NORMAL;
    if (CheckCollisionPointRec(GetMousePosition(), ButtonDimensions)) {
        ButtonState = BUTTONSTATE_FOCUSED;

        Result = 0;

        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT)) {
            ButtonState = BUTTONSTATE_DOWN;
            Result = 0;
        }
        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
            Result = 1;
        }
    }

    switch (ButtonState) {
        case BUTTONSTATE_NORMAL:
            DrawRectangle(ButtonDimensions.x, ButtonDimensions.y, ButtonDimensions.width, ButtonDimensions.height, NormalBorderColor); // Draw Borders
            DrawRectangle(ButtonDimensions.x + 5, ButtonDimensions.y + 5, ButtonDimensions.width - 10, ButtonDimensions.height - 10, NormalColor); // Draw Center
            DrawText(Text, ButtonDimensions.x + 5 + 2, ButtonDimensions.y + 5 + 2, FontSize, NormalTextColor);
            break;
        case BUTTONSTATE_FOCUSED:
            DrawRectangle(ButtonDimensions.x, ButtonDimensions.y, ButtonDimensions.width, ButtonDimensions.height, FocusedBorderColor); // Draw Borders
            DrawRectangle(ButtonDimensions.x + 5, ButtonDimensions.y + 5, ButtonDimensions.width - 10, ButtonDimensions.height - 10, FocusedColor); // Draw Center
            DrawText(Text, ButtonDimensions.x + 5 + 2, ButtonDimensions.y + 5 + 2, FontSize, FocusedTextColor);
            break;
        case BUTTONSTATE_DOWN:
            DrawRectangle(ButtonDimensions.x, ButtonDimensions.y, ButtonDimensions.width, ButtonDimensions.height, PressedBorderColor); // Draw Borders
            DrawRectangle(ButtonDimensions.x + 5, ButtonDimensions.y + 5, ButtonDimensions.width - 10, ButtonDimensions.height - 10, PressedColor); // Draw Center
            DrawText(Text, ButtonDimensions.x + 5 + 2, ButtonDimensions.y + 5 + 2, FontSize, PressedTextColor);
            break;
    }

    return Result;
}

QUI_API void qui_checkbox(Rectangle Dimensions, bool *State, const char *Text) {

    int CheckBoxState = CHECKBOXSTATE_NORMAL;

    if (CheckCollisionPointRec(GetMousePosition(), Dimensions)) {
        
        CheckBoxState = CHECKBOXSTATE_FOCUSED;

        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT)) CheckBoxState = CHECKBOXSTATE_DOWN;

        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) *State = !*State;
    }

    switch (CheckBoxState) {
        case CHECKBOXSTATE_NORMAL:
            DrawRectangle(Dimensions.x, Dimensions.y, Dimensions.width, Dimensions.height, I_QUITheme.SecondaryColor);
            DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth, Dimensions.y + I_QUITheme.BorderWidth, Dimensions.width - I_QUITheme.BorderWidth * 2, Dimensions.height - I_QUITheme.BorderWidth * 2, I_QUITheme.PrimaryColor);
            if (*State == true) {
                DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth + 5, Dimensions.y + I_QUITheme.BorderWidth + 5, Dimensions.width - I_QUITheme.BorderWidth * 4, Dimensions.height - I_QUITheme.BorderWidth * 4, GREEN);
            }
            else if (*State == false) {
                DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth + 5, Dimensions.y + I_QUITheme.BorderWidth + 5, Dimensions.width - I_QUITheme.BorderWidth * 4, Dimensions.height - I_QUITheme.BorderWidth * 4, RED);
            }
            break;

        case CHECKBOXSTATE_FOCUSED:
            DrawRectangle(Dimensions.x, Dimensions.y, Dimensions.width, Dimensions.height, (Color){I_QUITheme.SecondaryColor.r, I_QUITheme.SecondaryColor.g, I_QUITheme.SecondaryColor.b, I_QUITheme.SecondaryColor.a - 100});
            DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth, Dimensions.y + I_QUITheme.BorderWidth, Dimensions.width - I_QUITheme.BorderWidth * 2, Dimensions.height - I_QUITheme.BorderWidth * 2, (Color){I_QUITheme.PrimaryColor.r, I_QUITheme.PrimaryColor.g, I_QUITheme.PrimaryColor.b, I_QUITheme.PrimaryColor.a - 100});

            if (*State == true) {
                DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth + 5, Dimensions.y + I_QUITheme.BorderWidth + 5, Dimensions.width - I_QUITheme.BorderWidth * 4, Dimensions.height - I_QUITheme.BorderWidth * 4, (Color){0,255,0,155});
            }
            else if (*State == false) {
                DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth + 5, Dimensions.y + I_QUITheme.BorderWidth + 5, Dimensions.width - I_QUITheme.BorderWidth * 4, Dimensions.height - I_QUITheme.BorderWidth * 4, (Color){255,0,0,155});
            }
            break;
        case CHECKBOXSTATE_DOWN:
            DrawRectangle(Dimensions.x, Dimensions.y, Dimensions.width, Dimensions.height, I_QUITheme.PrimaryColor);
            DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth, Dimensions.y + I_QUITheme.BorderWidth, Dimensions.width - I_QUITheme.BorderWidth * 2, Dimensions.height - I_QUITheme.BorderWidth * 2, I_QUITheme.SecondaryColor);

            if (*State == true) {
                DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth + 5, Dimensions.y + I_QUITheme.BorderWidth + 5, Dimensions.width - I_QUITheme.BorderWidth * 4, Dimensions.height - I_QUITheme.BorderWidth * 4, GREEN);
            }
            else if (*State == false) {
                DrawRectangle(Dimensions.x + I_QUITheme.BorderWidth + 5, Dimensions.y + I_QUITheme.BorderWidth + 5, Dimensions.width - I_QUITheme.BorderWidth * 4, Dimensions.height - I_QUITheme.BorderWidth * 4, RED);
            }
            break;
    }
    DrawText(Text, Dimensions.x + Dimensions.width + 10, Dimensions.y + Dimensions.height / 2 - 10, 25, WHITE);
}

QUI_API bool qui_window(Rectangle *Dimensions, const char *Text, bool Dragable)  {

    bool Result;
    DrawRectangle(Dimensions->x, Dimensions->y, Dimensions->width, Dimensions->height, I_QUITheme.SecondaryColor);
    DrawRectangle(Dimensions->x, Dimensions->y, Dimensions->width, 34, I_QUITheme.DetailColor);

    if (qui_button((Rectangle){Dimensions->x + Dimensions->width - 25, Dimensions->y + 7, 20, 20}, "X", 10)) Result = true;
    else Result = false;

    DrawText(Text, Dimensions->x + 10, Dimensions->y + 7, 15, I_QUITheme.TextColor);

    if (Dragable) {
        if (CheckCollisionPointRec(GetMousePosition(), (Rectangle){Dimensions->x, Dimensions->y, Dimensions->width - 26, 34}) && IsMouseButtonDown(MOUSE_BUTTON_LEFT)) {
            Vector2 RawMousePos = GetMousePosition();
            Dimensions->x = RawMousePos.x - Dimensions->width / 2;
            Dimensions->y = RawMousePos.y - 34 / 2;
        }
    }

    return Result;

}

QUI_API void qui_init() {
    qui_reset_theme();
    printf("Quacked Up Debug UI Initialized\n");
}

QUI_API void qui_label(const char *Text, int PosX, int PosY, int FontSize) {
    DrawText(Text, PosX, PosY, FontSize, I_QUITheme.TextColor);
}

QUI_API void qui_slider(float *Value, float MaxValue, Rectangle Dimensions, const char *Text) {
    *Value = *Value;

    int SliderState = SLIDERSTATE_NORMAL;

    if (CheckCollisionPointRec(GetMousePosition(), Dimensions)) {
        SliderState = SLIDERSTATE_FOCUSED;

        if (IsMouseButtonDown(MOUSE_BUTTON_LEFT)) {
            *Value = ((GetMousePosition().x - Dimensions.x) / Dimensions.width) * MaxValue;
            SliderState = SLIDERSTATE_DOWN;
        }
    }

    if (*Value > MaxValue) *Value = MaxValue;

    switch (SliderState) {
        case SLIDERSTATE_NORMAL:
            DrawRectangle(Dimensions.x, Dimensions.y, Dimensions.width, Dimensions.height, I_QUITheme.DetailColor);
            DrawRectangle(Dimensions.x + 1, Dimensions.y + 1, *Value / MaxValue * Dimensions.width - 2, Dimensions.height - 2, I_QUITheme.PrimaryColor);
            break;
        
        case SLIDERSTATE_FOCUSED:
            DrawRectangle(Dimensions.x, Dimensions.y, Dimensions.width, Dimensions.height, (Color){I_QUITheme.DetailColor.r, I_QUITheme.DetailColor.g, I_QUITheme.DetailColor.b, 200});
            DrawRectangle(Dimensions.x + 1, Dimensions.y + 1, *Value / MaxValue * Dimensions.width - 2, Dimensions.height - 2, (Color){I_QUITheme.PrimaryColor.r, I_QUITheme.PrimaryColor.g, I_QUITheme.PrimaryColor.b, 200});
            break;
        
        case SLIDERSTATE_DOWN:
            DrawRectangle(Dimensions.x, Dimensions.y, Dimensions.width, Dimensions.height, I_QUITheme.TextColor);
            DrawRectangle(Dimensions.x + 1, Dimensions.y + 1, *Value / MaxValue * Dimensions.width - 2, Dimensions.height - 2, I_QUITheme.PrimaryColor);
    }

    qui_label(TextFormat("%f", *Value), Dimensions.x + Dimensions.width + 5, Dimensions.y + Dimensions.height / 2, 20);
    qui_label(Text, Dimensions.x, Dimensions.y + Dimensions.height + 2, 20);
}

#endif
// BorderColor
// DrawButton
// PopStyle
#endif
